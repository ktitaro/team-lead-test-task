# TeamLead Test Case

## Requirements

Generally you need only two things:

- docker
- docker-compose

## Usage

Running applications is simple, as:

```bash
$ docker-compose up
```

## Testing

There are two accounts covering two different roles.  
Use credentials provided bellow to login.

### Reader

- reader@mail.com
- 123456

### Writer

- writer@mail.com
- 123456

## Recommendations

Have fun 🤠