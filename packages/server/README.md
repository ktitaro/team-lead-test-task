# Server

A simply server which provides a public API. It uses json file as source of truth.

## Usage

If you want to use the server in the context of testing application, use the docker-compose in the root folder.

In any other case:

```bash
$ yarn install
$ yarn watch
```