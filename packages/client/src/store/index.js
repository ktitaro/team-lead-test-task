import Vue from 'vue'
import Vuex from 'vuex'

import {
  USER_LOGIN,
  USER_LOGOUT,
  LOADING_START,
  LOADING_FINISH,
} from '@/store/mutations'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: null,
    isLoading: false,
    isLoggedIn: false,
  },
  mutations: {
    USER_LOGIN: (state, user) => {
      state.user = user
      state.isLoggedIn = true
    },
    USER_LOGOUT: state => {
      state.user = null
      state.isLoggedIn = false
    },
    LOADING_START: state => {
      state.isLoading = true
    },
    LOADING_FINISH: state => {
      state.isLoading = false
    },
  },
  actions: {
    async withLoading({ commit }, callback) {
      commit(LOADING_START)
      await callback()
      commit(LOADING_FINISH)
    },
  },
})
