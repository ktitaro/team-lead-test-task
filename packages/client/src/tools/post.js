import axios from 'axios'

import User from '@/tools/user'

export default class Post {
  static async getPostsByPage(page) {
    try {
      const resp = await axios.get(`http://localhost:3000/posts/?_page=${page}`)
      const { status, statusText, data, headers } = resp
      if (status !== 200) {
        throw new Error(`Error occured while fetching posts: ${statusText}`)
      }
      return { data, headers }
    } catch (err) {
      throw new Error(`Error occured while fetching posts: ${err}`)
    }
  }

  static async getPostById(id) {
    try {
      const resp = await axios.get(`http://localhost:3000/posts/${id}/`)
      const { status, statusText, data } = resp
      if (status !== 200) {
        throw new Error(`Error occured while fetching post: ${statusText}`)
      }
      return data
    } catch (err) {
      throw new Error(`Error occured while fetching posts: ${err}`)
    }
  }

  static async getLatestPostId() {
    try {
      const { headers } = await this.getPostsByPage(1)
      return Number(headers['x-total-count'])
    } catch (err) {
      throw new Error(err.message)
    }
  }

  static async createPost({ title, description }) {
    try {
      const id = (await this.getLatestPostId()) + 1
      const { id: userId } = await User.getUserFromLocalStorage()
      const claps = 0
      const createdAt = new Date().toISOString()
      const updatedAt = new Date().toISOString()
      const resp = await axios.post('http://localhost:3000/posts/', {
        id,
        title,
        claps,
        userId,
        createdAt,
        updatedAt,
        description,
      })
      const { status, statusText, data } = resp
      if (status !== 201) {
        throw new Error(`Error occured while creating new post: ${statusText}`)
      }
      return data
    } catch (err) {
      throw new Error(err.message)
    }
  }

  static async updatePost(id, params) {
    try {
      await this.getPostById(id)
      const updatedAt = new Date().toISOString()
      const resp = await axios.patch(`http://localhost:3000/posts/${id}`, {
        ...params,
        updatedAt,
      })
      const { status, statusText, data } = resp
      if (status !== 200) {
        throw new Error(`Error occured while updating post: ${statusText}`)
      }
      return data
    } catch (err) {
      throw new Error(err.message)
    }
  }

  static async deletePost(id) {
    try {
      await this.getPostById(id)
      const { role } = await User.getUserFromLocalStorage()
      if (role === 'writer') {
        const resp = await axios.delete(`http://localhost:3000/posts/${id}`)
        const { status, statusText } = resp
        if (status !== 200) {
          throw new Error(`Error occured while deleting post: ${statusText}`)
        }
      } else {
        throw new Error('You do not have enough rights')
      }
    } catch (err) {
      throw new Error(err.message)
    }
  }

  static async clapToPost(id) {
    try {
      const post = await this.getPostById(id)
      const { role } = await User.getUserFromLocalStorage()
      if (role === 'reader') {
        const resp = await axios.patch(`http://localhost:3000/posts/${id}`, {
          claps: post.claps + 1,
        })
        const { status, statusText, data } = resp
        if (status !== 200) {
          throw new Error(`Error occured while clapping: ${statusText}`)
        }
        return data
      }
      await this.updatePost(id, { claps: '+=1' })
      return true
    } catch (err) {
      throw new Error(err.message)
    }
  }
}
