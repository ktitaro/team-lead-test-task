import axios from 'axios'

import store from '@/store'
import { USER_LOGIN, USER_LOGOUT } from '@/store/mutations'

export default class User {
  static setUserToLocalStorage(user) {
    try {
      localStorage.setItem('user', JSON.stringify(user))
    } catch (err) {
      throw new Error(
        `Error occured while setting user to localStorage: ${err}`
      )
    }
  }

  static getUserFromLocalStorage() {
    try {
      let user = localStorage.getItem('user')
      if (user) {
        user = JSON.parse(user)
        return user
      }
    } catch (err) {
      throw new Error(
        `Error occured while getting user from localStorage: ${err}`
      )
    }
  }

  static removeUserFromLocalStorage() {
    try {
      const user = this.getUserFromLocalStorage()
      if (user) {
        localStorage.removeItem('user')
      }
    } catch (err) {
      throw new Error(
        `Error occured while removing user from localStorage: ${err}`
      )
    }
  }

  static async fetchUserCredentials(login) {
    try {
      const resp = await axios.get(
        `http://localhost:3000/users/?login=${login}`
      )
      const { status, textStatus, data } = resp
      if (status !== 200) {
        throw new Error(
          `Error occured while fetching user credentials: ${textStatus}`
        )
      }
      if (data.length === 0) {
        throw new Error('User does not exists or credentials are wrong')
      }
      return data[0]
    } catch (err) {
      throw new Error(`Error occured while fetching user credentials: ${err}`)
    }
  }

  static async logoutUser() {
    try {
      await this.removeUserFromLocalStorage()
      store.commit(USER_LOGOUT)
    } catch (err) {
      throw new Error(`Error occured while logging out: ${err}`)
    }
  }

  static async loginUser(login, password) {
    try {
      const credentials = await this.fetchUserCredentials(login)
      if (String(password) === String(credentials.password)) {
        store.commit(USER_LOGIN, credentials)
        this.setUserToLocalStorage(credentials)
        return true
      } else {
        return false
      }
    } catch (err) {
      throw new Error(err.message)
    }
  }

  static async loadUserFromLocalStorage() {
    try {
      const user = await this.getUserFromLocalStorage()
      if (user) {
        const { login, password } = user
        await this.loginUser(login, password)
      }
    } catch (err) {
      throw new Error(err.message)
    }
  }
}
