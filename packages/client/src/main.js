import Vue from 'vue'
import axios from 'axios'
import Buefy from 'buefy'
import VueAxios from 'vue-axios'
import VueMoment from 'vue-moment'

import App from '@/App.vue'
import store from '@/store'
import router from '@/router'

import '@mdi/font/css/materialdesignicons.css'
import 'buefy/dist/buefy.css'

Vue.use(Buefy)
Vue.use(VueAxios, axios)
Vue.use(VueMoment)

Vue.config.productionTip = false

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')
