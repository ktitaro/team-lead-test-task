import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/pages/Home'

Vue.use(VueRouter)

export default new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/auth',
      name: 'auth',
      component: () =>
        import(/* webpackChunkName: "auth" */ '@/pages/Auth.vue'),
    },
    {
      path: '/post/',
      name: 'post-create',
      component: () =>
        import(/* webpackChunkName: "post-create" */ '@/pages/PostCreate.vue'),
    },
    {
      path: '/post/:id',
      name: 'post-edit',
      component: () =>
        import(/* webpackChunkName: "post-edit" */ '@/pages/PostEdit.vue'),
    },
  ],
})
